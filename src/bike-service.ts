import axios from "axios"
import { Bike } from "./entities";



export const bikeService = {
    async fetchAll() {
        const response = await axios.get<Bike[]>(process.env.NEXT_PUBLIC_SERVER_URL+'/api/bike');
        return response.data;
    },
    async post(bike:Bike) {
        const response = await axios.post<Bike>(process.env.NEXT_PUBLIC_SERVER_URL+'/api/bike', bike);
        return response.data;
    },
}