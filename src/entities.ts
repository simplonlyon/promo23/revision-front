export interface Bike {
    id?:number;
    model:string;
    brand:string;
    size:string;
    picture:string;
}