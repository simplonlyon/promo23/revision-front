import { bikeService } from "@/bike-service"
import { Bike } from "@/entities"
import { GetServerSideProps } from "next"
import { useState } from "react"

interface Props {
  bikes:Bike[]
}

export default function Home({bikes:initialBikes}:Props) {
  const [bikes, setBikes] =useState(initialBikes);
  const [bike,setBike] = useState<Bike>({
    brand:'',
    model:'',
    picture:'',
    size:'M'
  });
  function handleChange(event:any) {
    setBike({
      ...bike,
      [event.target.name]:event.target.value
    });

  }

  function handleFile(event:any) {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      setBike({
        ...bike,
        picture: fileReader.result as string
      })
    }
    fileReader.readAsDataURL(event.target.files[0])
    
  }

  async function handleSubmit(event:any) {
    event.preventDefault();

    const persisted = await bikeService.post(bike);
    setBikes([...bikes, persisted]);
  }

  function makeLink(link:string) {
    
    if(link.startsWith('http')) {
      return link;
    } 
    return process.env.NEXT_PUBLIC_SERVER_URL+'/uploads/'+link;
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label>
          Model : 
          <input type="text" onChange={handleChange} name="model" value={bike.model} />
        </label>


        <label>
          Size : 
          <select name="size" onChange={handleChange} value={bike.size}>
            <option>XS</option>
            <option>S</option>
            <option>M</option>
            <option>L</option>
            <option>XL</option>
          </select>
        </label>

        <label>
          Picture : 
          <input type="file" name="picture" onChange={handleFile} />
        </label>
        <button>Add</button>
      </form>
      {bikes.map(item => <p key={item.id}>{item.model} <img width={200} src={makeLink(item.picture)} alt={item.model} /></p>)}
    </>
  )
}

export const getServerSideProps:GetServerSideProps<Props> = async () => {
  return {
    props: {
      bikes: await bikeService.fetchAll()
    }
  }
}